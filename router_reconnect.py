#!/usr/bin/env python3
# Copyright (c) 2021 Lukas Fink
# Released under the MIT license. Read 'LICENSE' for more information.

import easybox602, time, requests

UN = 'username'
PW = 'password'

def log(msg):
	print('[{}] {}'.format(time.asctime(), msg))

def log_ip(msg, ip):
	log('{} IP: {}'.format(msg, ip))

s = easybox602.Session()

lastip = newip = requests.get('http://ifconfig.co/ip').text.strip()
log_ip('Started.', lastip)
while True:
	try:
		s.login(UN, PW)

		while lastip == newip:
			s.reconnect()
			newip = requests.get('http://ifconfig.co/ip').text.strip()
			log_ip('Reconnected.', newip)
		lastip = newip

		s.logout()
		time.sleep(900)
	except Exception as e:
		log(str(e) if e.message is None else e.message)
