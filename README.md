# EasyBox 602 library

This is a small library that allows to control the EasyBox 602’s web interface via Python scripts. It currently only allows to reconnect the DSL connection. As I’m not using this router any more, I don’t plan on adding any new feature. It still might be useful to someone.

`router_reconnect.py` is an example application that – from the looks of it – tries to acquire a new ip address every 900 seconds.

It’s quite a long time since I used any of this, so it may or may not work.
