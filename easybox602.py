#!/usr/bin/env python3
# Copyright (c) 2021 Lukas Fink
# Released under the MIT license. Read 'LICENSE' for more information.

import requests
from urllib.parse import urljoin, urlsplit
from html.parser import HTMLParser

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class AuthError(Error):
    """Raised when username or password for login were wrong.

    Attributes:
        message -- explanation of the error
        username -- given username
        password -- given password
    """

    def __init__(self, username, password, message=None):
        if message == None:
            super().__init__('Wrong username "' + username + '" or password "' + password + '".')
        else:
            super().__init__(message)

        self.username = username
        self.password = password

class InUsageError(Error):
    """Raised when tried to login while another device is logged in.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message=None):
        if message == None:
            super().__init__('Another device is logged in.')
        else:
            super().__init__(message)

class NotLoggedInError(Error):
    """Raised when tried to perform an action while not being logged in.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message=None):
        if message == None:
            super().__init__('Not logged in.')
        else:
            super().__init__(message)

class RedirectError(Error):
    """Raised when redirected to unexpected url.

    Attributes:
        message -- explanation of the error
        url -- redirected url
    """

    def __init__(self, url, message=None):
        if message == None:
            super().__init__('Redirected to unexpected url "' + url + '".')
        else:
            super().__init__(message)

        self.url = url

class Session(requests.Session):
    """Session on a easybox 602 device."""

    def __init__(self, address=None):
        super().__init__()
        if address == None:
            self.address = 'http://easy.box'
        else:
            self.address = address

    def request(self, method, url, *args, **kwargs):
        kwargs['allow_redirects'] = False
        return super().request(method, urljoin(self.address, url), *args, **kwargs)

    def close(self):
        self.logout()
        return super().close()

    def login(self, username, password):
        r = self.post('/cgi-bin/login.exe', data={
            'user': username,
            'pws': password})

        actionerrors(r, '/index.stm', {
            '/loginpserr.stm': AuthError(username, password),
            '/loginerr.stm': InUsageError()})

    def logout(self):
        self.get('/cgi-bin/logout.exe')

    def isloggedin(self):
        r = self.get('/cgi-bin/dsl_log.log')
        return r.headers['Content-type'] != 'text/html'

    def reconnect(self):
        r = self.post('/cgi-bin/statusprocess.exe', data={
            'pvc': '0',
            'cur_if': '11',
            'disconnect.x': '0',
            'disconnect.y': '0'})
        actionerrors(r, '/status_main.stm')

def actionerrors(resp, expectpath=None, errors={}):
    if resp.status_code != 302:
        raise NotLoggedInError()
    redirurl = resp.headers['Location']
    redirpath = urlsplit(redirurl)[2]

    try:
        raise errors[redirpath]
    except KeyError:
        pass

    if expectpath != None and redirpath != expectpath:
        raise RedirectError(redirurl)
